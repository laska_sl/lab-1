﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace NoteBookApp
{
    class Notebook
    {
        List<Note> notes = new List<Note>();

        private Note this[int ID] //ИНДЕКСАТОР ДЛЯ ОБРАЩЕНИЯ К ЗАПИСКАМ ПО ПОЛЮ ID
        {
            get
            {
                foreach (Note note in notes)
                {
                    if (note.ID == ID)
                    {
                        return note;
                    }
                }
                return null;
            }
        }
        public void UI()
        {
            Console.Clear();

            ShowAll();

            Console.WriteLine("1 - Add note\n2 - Edit note\n3 - Delete note\n4 - Read note\n");
            Console.Write(">");

            string choice = Console.ReadLine();
            switch (choice)
            {
                case "1":
                    AddNote();
                    break;

                case "2":
                    EditNote();
                    break;

                case "3":
                    DeleteNote();
                    break;

                case "4":
                    Console.Clear();
                    Console.WriteLine("Enter ID of note you want to read: ");
                    Console.Write(">");
                    int read_id = Int32.Parse(Console.ReadLine());
                    ReadNote(read_id);
                    break;

                default:
                    Console.WriteLine("There is no such procedure! Try again...");
                    break;
            }
        }

        private void AddNote()
        {
            Console.Clear();
            Console.WriteLine("Enter information of note you want to create. Required field are marked '*' ");

            Console.WriteLine("\nEnter firstname*: ");
            Console.Write(">");
            string firstname = CheckRequiredField(Console.ReadLine());

            Console.WriteLine("\nEnter secondname*: ");
            Console.Write(">");
            string secondname = CheckRequiredField(Console.ReadLine());

            Console.WriteLine("\nEnter middlename: ");
            Console.Write(">");
            string middlename = Console.ReadLine();

            Console.WriteLine("\nEnter phone number*: ");
            Console.Write(">");
            string phone = CheckRequiredField(Console.ReadLine());

            Console.WriteLine("\nEnter country*");
            Console.Write(">");
            string country = CheckRequiredField(Console.ReadLine());

            Console.WriteLine("\nEnter Date of Birth: ");
            Console.Write(">");
            string birth = Console.ReadLine();

            Console.WriteLine("\nEnter organization: ");
            Console.Write(">");
            string organization = Console.ReadLine();

            Console.WriteLine("\nEnter position: ");
            Console.Write(">");
            string position = Console.ReadLine();

            Console.WriteLine("\nEnter addition notes: ");
            Console.Write(">");
            string additionNotes = Console.ReadLine();

            Note note = new Note(firstname, secondname, middlename, phone, country, birth, organization, position, additionNotes);
            notes.Add(note);

            Console.WriteLine("\nEnter to continue...");
            Console.ReadKey();
        }

        private void EditNote()
        {
            Console.Clear();
            ShowAll();

            Console.WriteLine("Enter ID of note you want to edit: ");
            Console.Write(">");
            int edit_id = Int32.Parse(Console.ReadLine());

            if (this[edit_id]!=null)
            {
                Console.WriteLine("Choose the field you want to edit: ");
                FieldInfo[] fieldInfos = notes[edit_id].GetType().GetFields();
                for (int i = 0; i < fieldInfos.Length; i++)
                {
                    Console.WriteLine("{0} - {1}", i + 1, fieldInfos[i].Name.ToUpper());
                }
                Console.Write(">");
                int choice = Int32.Parse(Console.ReadLine());
                var field = this[edit_id].GetType().GetField(fieldInfos[choice-1].Name);

                field.SetValue(this[edit_id], SetNewValue());
            }
            else
            {
                Console.WriteLine("There's no note with such ID!!!");
                Console.WriteLine("\nEnter to continue...");
                Console.ReadKey();
            }            
        }
        private void DeleteNote()
        {
            Console.Clear();
            ShowAll();

            Console.WriteLine("Enter ID of note you want to remove from list: ");
            Console.Write(">");
            int delete_id = Int32.Parse(Console.ReadLine());


            if (this[delete_id]!=null)
            {
                Console.Clear();
                Console.WriteLine("You are going to delete this note:");
                ReadNote(delete_id);
                Console.WriteLine("Right?\n1 - Yes!\n2 - No!");
                Console.Write(">");
                switch (Console.ReadLine())
                {
                    case "1":
                        notes.Remove(this[delete_id]);
                        Console.Clear();
                        Console.WriteLine("Deleted successfully!");
                        break;
                    case "2":
                    default:
                        break;
                }
            }
            else
            {
                Console.WriteLine("There's no note with such ID!!!");
            }

            Console.WriteLine("\nEnter to continue...");
            Console.ReadKey();
        }
        private void ReadNote(int id)
        {
            if (this[id] != null)
            {
                Console.WriteLine(" ID |       FIRSTNAME |      SECONDNAME |      MIDDLENAME |       PHONE |    COUNTRY | DATE OF BIRTH |    ORGANIZATION |        POSITION | ADDITION NOTES");
                Console.WriteLine(this[id].ToString());
            }
            else
            {
                Console.WriteLine("There's no note with such ID!!! Enter to continue...");
                Console.ReadKey();
            }


        }
        private void ShowAll()
        {
            if (notes.Count == 0)
            {
                Console.WriteLine("NoteBook is empty :(\n");
                return;
            }

            Console.WriteLine(" ID |       FIRSTNAME |      SECONDNAME |      MIDDLENAME |       PHONE |    COUNTRY | DATE OF BIRTH |    ORGANIZATION |        POSITION | ADDITION NOTES");
            foreach (Note note in notes)
            {
                Console.WriteLine(note.ToString());
            }
            Console.WriteLine();
        }

        private string CheckRequiredField(string field)
        {
            while (String.IsNullOrEmpty(field))
            {
                Console.WriteLine("This field is required!!! Try again...");
                Console.Write(">");
                field = Console.ReadLine();
            }
            return field;
        }

        private string SetNewValue()
        {
            Console.WriteLine("Enter new value: ");
            Console.Write(">");
            string new_value = Console.ReadLine();
            return new_value;
        }
    }
}
