﻿using System;

namespace NoteBookApp
{
    class Note
    {
        public int ID { get; private set; }
        public string firstname;   
        public string secondname;
        public string middlename;
        public string phone;
        public string country;
        public string birth;
        public string organization;
        public string position;
        public string notes;

        static int count; //ДЛЯ СОЗДАНИЯ УНИКАЛЬНЫХ НЕИЗМЕНЯЕМЫХ ID

        public object this[string fieldName] //ИНДЕКСАТОР ДЛЯ ОБРАЩЕНИЯ К ПОЛЯМ ЭКЗЕМПЛЯРА NOTE ПО ИМЕНИ
        {
            get
            {
                var field = this.GetType().GetField(fieldName);
                return field.GetValue(this);
            }
            set
            {
                var field = this.GetType().GetField(fieldName);
                field.SetValue(this, value);
            }
        }

        public Note(string firstname, string secondname, string middlename, string phone, string country, string birth, string organization, string position, string notes)
        {
            ID = count;
            this.firstname = firstname;
            this.secondname = secondname;
            this.middlename = middlename;
            this.phone = phone;
            this.country = country;
            this.birth = birth;
            this.organization = organization;
            this.position = position;
            this.notes = notes;

            count++;
        }
                
        public override string ToString()
        {
            return String.Format("{0,3} | {1,15} | {2,15} | {3,15} | {4,11} | {5,10} | {6,13} | {7,15} | {8,15} | {9}", ID, firstname, secondname, middlename, phone, country, birth, organization, position, notes);
        }
    }
}
